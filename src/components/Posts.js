import React from 'react';
import Post from './Post';
import {connect} from 'react-redux'

 function Posts ({data}) {

    if(!data.length){
        return <p>empty</p>
    }
    return(
        data.map(post => <Post post={post} key={post.id}/>)
    )
}

const mapStateToProps = state => {
    console.log(state);

    return{
        data: state.posts.data
    }
}
export default connect(mapStateToProps, null) (Posts);